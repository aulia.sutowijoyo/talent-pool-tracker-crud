import { GET_USER_BEGIN, GET_PIC_BEGIN, GET_COMPANY_BEGIN, GET_TRACKER_BEGIN} from "../const/type";

export const getUser = () => {
  return {
    type: GET_USER_BEGIN,
  };
};
export const getCompany = () => {
  return {
    type: GET_COMPANY_BEGIN,
  };
};
export const getPIC = () => {
  return {
    type: GET_PIC_BEGIN,
  };
};

export const getTRACKER = () => {
  return {
    type: GET_TRACKER_BEGIN,
  };
};