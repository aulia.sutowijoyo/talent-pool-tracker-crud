/* eslint-disable no-unused-vars */
import axios from "axios";
import { GET_TRACKER_FAIL, GET_TRACKER_SUCCESS, GET_TRACKER_BEGIN, GET_USER_FAIL, GET_USER_SUCCESS, GET_USER_BEGIN, GET_PIC_BEGIN, GET_PIC_FAIL, GET_PIC_SUCCESS,GET_COMPANY_BEGIN, GET_COMPANY_FAIL, GET_COMPANY_SUCCESS, } from "../const/type";
import { put, takeEvery } from "redux-saga/effects";
// import Swal from "sweetalert2/dist/sweetalert2.js";


function* getUser(actions) {
    const { error } = actions;
    try {
      const res = yield axios.get(`
      https://glints-team-project-assessment.herokuapp.com/talents`, {headers: {
        'Accept': 'application/json', 
        'Content-Type': 'application/x-www-form-urlencoded'  
    }
    });
      yield put({
        type: GET_USER_SUCCESS,
        payload: res.data,
      });
    } catch (error) {
      yield put({
        type: GET_USER_FAIL,
        payload: error,
      });
    }
  }

export function* watchGetUser() {
    yield takeEvery(GET_USER_BEGIN, getUser);
  }

  function* getCompany(actions) {
    const { error } = actions;
    try {
      const res = yield axios.get(`
      https://glints-team-project-assessment.herokuapp.com/companies`, {headers: {
        'Accept': 'application/json', 
        'Content-Type': 'application/x-www-form-urlencoded'  
    }
    });
      yield put({
        type: GET_COMPANY_SUCCESS,
        payload: res.data,
      });
    } catch (error) {
      yield put({
        type: GET_COMPANY_FAIL,
        payload: error,
      });
    }
  }

export function* watchGetCompany() {
    yield takeEvery(GET_COMPANY_BEGIN, getCompany);
  }

 

  function* getPIC(actions) {
    const { error } = actions;
    try {
      const res = yield axios.get(`
      https://glints-team-project-assessment.herokuapp.com/pic`, {headers: {
        'Accept': 'application/json', 
        'Content-Type': 'application/x-www-form-urlencoded'  
    }
    });
      yield put({
        type: GET_PIC_SUCCESS,
        payload: res.data,
      });
    } catch (error) {
      yield put({
        type: GET_PIC_FAIL,
        payload: error,
      });
    }
  }

export function* watchGetPIC() {
    yield takeEvery(GET_PIC_BEGIN, getPIC);
  }

  function* getTRACKER(actions) {
    const { error } = actions;
    try {
      const res = yield axios.get(`
      https://glints-team-project-assessment.herokuapp.com/trackers`, {headers: {
        'Accept': 'application/json', 
        'Content-Type': 'application/x-www-form-urlencoded'  
    }
    });
      yield put({
        type: GET_TRACKER_SUCCESS,
        payload: res.data,
      });
    } catch (error) {
      yield put({
        type: GET_TRACKER_FAIL,
        payload: error,
      });
    }
  }

export function* watchGetTracker() {
    yield takeEvery(GET_TRACKER_BEGIN, getTRACKER);
  }