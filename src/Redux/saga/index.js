import { all } from "@redux-saga/core/effects";
import { watchGetUser, watchGetCompany,watchGetPIC, watchGetTracker} from "./userSaga";

export default function* rootSaga() {
    // function generator
    yield all([
      watchGetUser(),watchGetCompany(),watchGetPIC(), watchGetTracker()
    
    ]);
  }
  