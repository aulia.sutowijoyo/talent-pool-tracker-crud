import { GET_TRACKER_FAIL, GET_TRACKER_SUCCESS, GET_TRACKER_BEGIN } from "../const/type";

const initialState = {
    trackerInfo: {
        tracker: [],
    loading: false,
    error: null,
  },
 
};

const trackerData = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    default:
      return {
        ...state,
      };
    
    case GET_TRACKER_BEGIN:
      return {
        ...state,
        trackerInfo: {
          loading: true,
          // error: null,
        },
      };
    case GET_TRACKER_SUCCESS:
      return {
        ...state,
        trackerInfo: {
            tracker: payload,
          loading: false,
          error: null,
        },
      };
    case GET_TRACKER_FAIL:
      return {
        trackerInfo: {
          tracker: [],
          loading: false,
          error: error,
        },
      };
  }
};

export default trackerData;
