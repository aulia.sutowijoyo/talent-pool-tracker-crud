import { GET_COMPANY_FAIL, GET_COMPANY_SUCCESS, GET_COMPANY_BEGIN } from "../const/type";

const initialState = {
  companyInfo: {
    company: [],
    loading: false,
    error: null,
  },
 
};

const companyData = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    default:
      return {
        ...state,
      };
    
    case GET_COMPANY_BEGIN:
      return {
        ...state,
        companyInfo: {
          loading: true,
          // error: null,
        },
      };
    case GET_COMPANY_SUCCESS:
      return {
        ...state,
        companyInfo: {
          company: payload,
          loading: false,
          error: null,
        },
      };
    case GET_COMPANY_FAIL:
      return {
        companyInfo: {
          company: [],
          loading: false,
          error: error,
        },
      };
  }
};

export default companyData;
