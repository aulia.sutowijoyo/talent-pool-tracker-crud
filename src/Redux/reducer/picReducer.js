import { GET_PIC_FAIL, GET_PIC_SUCCESS, GET_PIC_BEGIN } from "../const/type";

const initialState = {
  picInfo: {
    pic: [],
    loading: false,
    error: null,
  },
 
};

const picData = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    default:
      return {
        ...state,
      };
    
    case GET_PIC_BEGIN:
      return {
        ...state,
        picInfo: {
          loading: true,
        },
      };
    case GET_PIC_SUCCESS:
      return {
        ...state,
        picInfo: {
          pic: payload,
          loading: false,
          error: null,
        },
      };
    case GET_PIC_FAIL:
      return {
        picInfo: {
          pic: [],
          loading: false,
          error: error,
        },
      };
  }
};

export default picData;
