import { combineReducers } from "redux";
import userData from "./userReducer";
import picData from "./picReducer";
import companyData from "./companyReducer";
import trackerData from "./trackerReducer";




export default combineReducers({
    userData,picData,companyData,trackerData
  });