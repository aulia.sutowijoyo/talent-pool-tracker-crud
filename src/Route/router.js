import React from 'react'
import { Switch, Route } from "react-router-dom";
import UserMain from '../pages/UserPage/UserMain';

function Router() {
    return (
        <>
        {/* <Bar /> */}
        <Switch>
      
          <Route exact path="/">
            <UserMain />
          </Route>
   
          <Route path="*">
            <div>
              <h1>Not Found :(</h1>
            </div>
          </Route>
        </Switch>
      </>
    )
}

export default Router
