import React, {useState} from 'react'
import PlusIcon from '../../assets/Add.svg'
import {getCompany} from '../../Redux/action/user'
import {Form,Modal, Button} from 'react-bootstrap';
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import './UserMain'
import './Career.scss'
import Swal from 'sweetalert2'


function Company() {
const { company } = useSelector((state) => state.companyData.companyInfo);

const dispatch = useDispatch();
// const access_token = localStorage.getItem("access_token");
const [modalShow, setModalShow] = useState(false);
console.log('data perusahaan',company)


const [state, setState] = useState({
name: "",
stackNeeded : "",
experienceNeeded : "",
})

const [idUser, setIdUser] = useState()
const [addItem, setaddItem] = useState(false)
// ===========Add Item==================
const Addbegin = () => {
    if (state.name === "" || state.stackNeeded === ""|| state.experienceNeeded === '') {
    Swal.fire({
        icon: 'warning',
        width: 450,
        confirmButtonText: "ok",
        // confirmButtonColor: "#FF8888",
        // allowEnterKey: false,
        // showCancelButton: true,
        // cancelButtonText: 'Cancel',
        title: 'please fill all input',
        // text: 'Click Outside if you want to Cancel',
        })}else {
            AddCompany()
        }};

const AddCompany = async (e) => {
// eslint-disable-next-line no-unused-vars
try { const res = await axios.post(`
https://glints-team-project-assessment.herokuapp.com/companies`, state,

).then((res) => {
dispatch(getCompany())
setaddItem(false)

});
} catch (error) {
console.log('error apa ini',error);
if (error) {
console.log('error loh',error)

Swal.fire({
position: 'center',
icon: 'error',
title: 'Error',
// text: error.response.data.error.errors,
showConfirmButton: false,
timer: 2000
})}}};
// ==============Add End==================
// ===============Edit====================
const EditCompany = async (e) => {
// eslint-disable-next-line no-unused-vars
try { const res = await axios.put(`
https://glints-team-project-assessment.herokuapp.com/companies/${idUser}`, state,

).then((res) => {
dispatch(getCompany())
setModalShow(false)

});
} catch (error) {
console.log('error apa ini',error);
if (error) {

Swal.fire({
position: 'center',
icon: 'error',
title: 'Error',
// text: error.response.data.error.errors,
showConfirmButton: false,
timer: 2000
})
}
}
};
// ===============Delete====================
// constDeleteBegin
const DeleteBegin = () => Swal.fire({
    icon: 'warning',
    width: 450,
    confirmButtonText: "Delete??",
    confirmButtonColor: "#FF8888",
    allowEnterKey: false,
    showCancelButton: true,
    cancelButtonText: 'Cancel',
    title: 'Are You Sure!?',
    // text: 'Click Outside if you want to Cancel',
    }).then((result) => {
      if (result.isConfirmed) {
        DeleteCompany()
       } 
    });

const DeleteCompany = async (e) => {
// eslint-disable-next-line no-unused-vars
try { const res = await axios.delete(`
https://glints-team-project-assessment.herokuapp.com/companies/${idUser}`, 

).then((res) => {
dispatch(getCompany())
setModalShow(false)

});
} catch (error) {
console.log('error apa ini',error);
if (error) {

Swal.fire({
position: 'center',
icon: 'error',
title: 'Error',
// text: error.response.data.error.errors,
showConfirmButton: false,
timer: 2000
})
}
}
};


return (
<div className='InfoContainer'>
<div className='CareerContainer'>
        <div className='TitleTopContainer mb-4'>
            <div className='TitlePlace' style={{width:'100%', }}>
                <div className='TitleText'>Company List</div>
                <div className='SubTitleText'>List of Great Company</div>
            </div>
            <img onClick={()=> {setaddItem(true); setState({
            name: "",
            stackNeeded : "",
            experienceNeeded : 0,
            })}}
            src={`${PlusIcon}`} alt=''></img>
            {/* ========================Add ===================== */}
        </div>
        { addItem ?
        <div className='UpdateCareer'>
            <div className='InfoText'>Company Name</div>
            <Form.Group className="" controlId="">
                <Form.Control style={{borderColor:'#5C7AE5'}} required className="FormInput" type="text"
                    placeholder="Name" value={state.name} onChange={(e)=> setState({ ...state, name: e.target.value })}
                    />
            </Form.Group>
            <div className='InfoText'>Stack Needed</div>

            <Form.Select value={state.stackNeeded} onChange={(e)=> setState({ ...state, stackNeeded: e.target.value })}
                className="Dropdown me-sm-2" id="PilihSatuan"
                // style={{ borderRadius: "10px" }}
                >
                <option id="PilihItem" value="Backend">
                    Backend
                </option>
                <option id="PilihItem" value="Frontend">
                    Frontend
                </option>
                <option id="PilihItem" value="React Native">
                    React Native
                </option>
                <option id="PilihItem" value="QA">
                    QA
                </option>

            </Form.Select>

            <div className='InfoText'>Experience Needed</div>
            <Form.Group className="" controlId="">
                <Form.Control required className="DateInput" type="number" placeholder="Years" value={state.experienceNeeded}
                    onChange={(e)=> setState({ ...state, experienceNeeded: e.target.value })}
                    />
            </Form.Group>
            <div style={{marginBottom:'2rem', backgroundColor:'#5C7AE5', width:'100%'}} className="SeperateLine"></div>

            <div className='ButtonCell' style={{marginTop:'2rem'}}>
                <div>
                <Button variant='danger' style={{float:'right'}} onClick={()=> setaddItem(false)}>Cancel</Button>
                </div>
                <Button style={{width:'10rem', lineHeight:'1rem'}} variant="dark" onClick={()=> {Addbegin()}}
                        className='LoginButton' > Add Company</Button>
            </div>
        </div>
        : null }

        {/* ========================Mapping===================== */}

        <div className='TableList'>

            <div className='ContentContainer'>
      
                <div className='MainContent'>
                    <div className='MainTitle d-flex justify-content-between'>
                        {/* <div className='TitleBox'>Company Name</div>
                        <div className='TitleBox'>Stack Needed</div>
                        <div className='TitleBox'>Experience Needed</div> */}

                    </div>
                    <div style={{marginBottom:'2rem', backgroundColor:'#5C7AE5', width:'100%'}}
                        className="SeperateLine"></div>
 <div className="mappingBox">
                        {company?.data?.map((company, index)=>(
                        <div className="box" onClick={()=>
                            {setModalShow(true) ;setIdUser(company?.id);setState({
                                name: company?.name,
                                stackNeeded : company?.stackNeeded,
                                experienceNeeded : company?.experienceNeeded,
                            })}}>
                            <h3 style={{padding:'0', margin:'0'}}> {company?.name}</h3>
                            <div>Stack needed: {company?.stackNeeded}</div>
                            <div>Exp. Needed: {company?.experienceNeeded} tahun</div>

                        </div>
                        ))}
                    </div>
                        
                
                </div>

            </div>
        </div>

        {/* ========================Modal Edit ===================== */}

        <Modal className='EditModal' size="md" aria-labelledby="contained-modal-title-vcenter" centered show={modalShow}
            onHide={()=> setModalShow(false)}
            >
            <Modal.Body>
                <div className='UpdateCareer'>
                    {company?.data?.filter((data)=> data?.id === idUser).map((item,index)=>(<h1 style={{color:'white'}}>{item?.name}</h1>))}
                    <div className='InfoText'>Company Name</div>
                    <Form.Group className="" controlId="">
                        <Form.Control style={{borderColor:'#5C7AE5'}} required className="FormInput" type="text"
                            placeholder="Name" value={state.name} onChange={(e)=> setState({ ...state, name:
                            e.target.value })}
                            />
                    </Form.Group>
                    <div className='InfoText'>Stack Needed</div>

                    <Form.Select value={state.stackNeeded} onChange={(e)=> setState({ ...state, stackNeeded:
                        e.target.value })}
                        className="Dropdown me-sm-2" id="PilihSatuan"
                        >
                        <option id="PilihItem" value="Backend">
                            Backend
                        </option>
                        <option id="PilihItem" value="Frontend">
                            Frontend
                        </option>
                        <option id="PilihItem" value="React Native">
                            React Native
                        </option>
                        <option id="PilihItem" value="QA">
                            QA
                        </option>
                    </Form.Select>

                    <div className='InfoText'>Experience Needed</div>
                    <Form.Group className="" controlId="">
                        <Form.Control required className="DateInput FormInput" type="number" placeholder="Years"
                            value={state.experienceNeeded} onChange={(e)=> setState({ ...state, experienceNeeded: e.target.value })}
                            />
                    </Form.Group>
                    <div style={{marginBottom:'2rem', backgroundColor:'#5C7AE5', width:'100%'}}
                        className="SeperateLine"></div>

                    <div className='ButtonCell' style={{marginTop:'2rem'}}>
                        <div>
                            
                        <Button variant='danger' style={{float:'right'}} onClick={()=> DeleteBegin()}>Delete Company</Button>

                        </div>
                        <Button style={{width:'10rem', lineHeight:'1rem'}} variant="dark" onClick={()=>
                                {EditCompany()}}
                                className='LoginButton' > Edit Company</Button>
                    </div>
                    <Button variant='danger' style={{float:'right'}} onClick={()=> setModalShow(false)}>Close</Button>
                </div>
            </Modal.Body>

        </Modal>



    </div>

</div>

)
}

export default Company