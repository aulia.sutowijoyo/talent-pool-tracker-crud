/* eslint-disable no-unused-vars */
import React, {useState} from 'react'
import PlusIcon from '../../assets/Add.svg'
import {getPIC, getUser} from '../../Redux/action/user'
import { Form, Col, Modal, Button} from 'react-bootstrap';
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import Swal from 'sweetalert2';
import Edit from '../../assets/edit.svg'



function PIC() {
    const { pic } = useSelector((state) => state.picData.picInfo);
    const dispatch = useDispatch();
    const [modalShow, setModalShow] = useState(false);
    
    
    const [state, setState] = useState({
    name: "",
    
    })
    
    const [idUser, setIdUser] = useState()
    const [addItem, setaddItem] = useState(false)
    // ===========Add ==========================
    const Addbegin = () => {
        if (state.name === "" ) {
        Swal.fire({
            icon: 'warning',
            width: 450,
            confirmButtonText: "ok",
                   title: 'please fill all input',
            })}else {
                AddPic()
            }};

    const AddPic = async (e) => {
    // eslint-disable-next-line no-unused-vars
    try { const res = await axios.post(`
    https://glints-team-project-assessment.herokuapp.com/pic`, state,
    
    ).then((res) => {
    dispatch(getPIC())
    setaddItem(false)
    
    });
    } catch (error) {
    console.log('error apa ini',error);
    if (error) {
    console.log('error loh',error)
    
    Swal.fire({
    position: 'center',
    icon: 'error',
    title: 'Error',
    // text: error.response.data.error.errors,
    showConfirmButton: false,
    timer: 2000
    })
    }
    }
    };
    // ===============Edit====================
    const EditPIC = async (e) => {
    // eslint-disable-next-line no-unused-vars
    try { const res = await axios.put(`
    https://glints-team-project-assessment.herokuapp.com/pic/${idUser}`, state,
    
    ).then((res) => {
    dispatch(getPIC())
    setModalShow(false)
    
    });
    } catch (error) {
    console.log('error apa ini',error);
    if (error) {
    
    Swal.fire({
    position: 'center',
    icon: 'error',
    title: 'Error',
    // text: error.response.data.error.errors,
    showConfirmButton: false,
    timer: 2000
    })
    }
    }
    };
    // ===============Delete====================
    // constDeleteBegin
    const DeleteBegin = () => Swal.fire({
        icon: 'warning',
        width: 450,
        confirmButtonText: "Delete??",
        confirmButtonColor: "#FF8888",
        allowEnterKey: false,
        showCancelButton: true,
        cancelButtonText: 'Cancel',
        title: 'Are You Sure!?',
        // text: 'Click Outside if you want to Cancel',
        }).then((result) => {
          if (result.isConfirmed) {
            DeletePIC()
           } 
        });
    
    const DeletePIC = async (e) => {
    // eslint-disable-next-line no-unused-vars
    try { const res = await axios.delete(`
    https://glints-team-project-assessment.herokuapp.com/pic/${idUser}`, 
    
    ).then((res) => {
    dispatch(getPIC())
    setModalShow(false)
    
    });
    } catch (error) {
    console.log('error apa ini',error);
    if (error) {
    
    Swal.fire({
    position: 'center',
    icon: 'error',
    title: 'Error',
    // text: error.response.data.error.errors,
    showConfirmButton: false,
    timer: 2000
    })
    }
    }
    };
    
return (
    <div className='InfoContainer'>
   <div className='CareerContainer'>
        <div className='TitleTopContainer mb-4'>
            <div className='TitlePlace' style={{width:'100%', }}>
                <div className='TitleText'>PIC List</div>
                <div className='SubTitleText'>List of Great PIC</div>
            </div>
            <img onClick={()=> {setaddItem(true); setState({
            name: "",
          
            })}}
            src={`${PlusIcon}`} alt=''></img>
            {/* ========================Add ===================== */}
        </div>
        { addItem ?
        <div className='UpdateCareer'>
            <div className='InfoText'>PIC Name</div>
            <Form.Group className="" controlId="">
                <Form.Control style={{borderColor:'#5C7AE5'}} required className="FormInput" type="text"
                    placeholder="Name" value={state.name} onChange={(e)=> setState({ ...state, name: e.target.value })}
                    />
            </Form.Group>
          
            <div style={{marginBottom:'2rem', backgroundColor:'#5C7AE5', width:'100%'}} className="SeperateLine"></div>

            <div className='ButtonCell' style={{marginTop:'2rem'}}>
                <div>
                <Button variant='danger' style={{float:'right'}} onClick={()=> setaddItem(false)}>Cancel</Button>

                </div>

                <Button style={{width:'10rem', lineHeight:'1rem'}} variant="dark" onClick={()=> {Addbegin()}}
                        className='LoginButton' > Add PIC</Button>
            </div>
        </div>
        : null }

        {/* ========================Mapping===================== */}

        <div className='TableList'>

            <div className='ContentContainer'>
      
                <div className='MainContent'>
                    <div className='MainTitle d-flex justify-content-between'>
                        <div className='TitleBox'>PIC Name</div>

                    </div>
                    <div style={{marginBottom:'2rem', backgroundColor:'#5C7AE5', width:'100%'}}
                        className="SeperateLine"></div>
                    <div className='d-flex justify-content-center'>
                        <ol> {pic?.data?.map((pic, index)=>(

                            <div className='d-flex justify-content-between'>
                                {/* <ol>
                                    <li>{talent?.index}</li>
                                </ol> */}
                                {/* <div className='ColumnContent' style={{width:'20%'}}>
                                <li key={index}>{pic?.name}</li>
                                </div> */}
                                <div className='ColumnContent' style={{width:'7rem'}}>
                                <li key={index}>{pic?.name}</li>
                                </div>
                                {/* <Col key={index} className='ColumnContent ' style={{display:'flex', justifyContent:'center'}}>
                                {pic?.idTalent?.name}</Col> */}
                                <Col key={index} className='ColumnContentNumber'>
                               <img style={{height:'20px',width:'20px', marginLeft:'2rem', cursor:'pointer'}} onClick={()=>
                                {setModalShow(true) ;setIdUser(pic?.id);setState({
                                name: pic?.name,
                                
                                })}}

                                alt=''
                                src={`${Edit}`}/>
                                </Col>

                            </div>
                            )
                            )}</ol>
                    </div>
                </div>

            </div>
        </div>

        {/* ========================Modal Edit ===================== */}

        <Modal className='EditModal' size="md" aria-labelledby="contained-modal-title-vcenter" centered show={modalShow}
            onHide={()=> setModalShow(false)}
            >
            <Modal.Body>
                <div className='UpdateCareer'>
                    {pic?.data?.filter((data)=> data?.id === idUser).map((item,index)=>(<h1 style={{color:'white'}}>{item?.name}</h1>))}
                    <div className='InfoText'>PIC Name</div>
                    <Form.Group className="" controlId="">
                        <Form.Control style={{borderColor:'#5C7AE5'}} required className="FormInput" type="text"
                            placeholder="Name" value={state.name} onChange={(e)=> setState({ ...state, name:
                            e.target.value })}
                            />
                    </Form.Group>
                 
                    {/* <div className='InfoText'>Experience Needed</div>
                    <Form.Group className="" controlId="">
                        <Form.Control required className="DateInput FormInput" type="number" placeholder="Years"
                            value={state.experience} onChange={(e)=> setState({ ...state, experienceNeeded: e.target.value })}
                            />
                    </Form.Group> */}
                    <div style={{marginBottom:'2rem', backgroundColor:'#5C7AE5', width:'100%'}}
                        className="SeperateLine"></div>

                    <div className='ButtonCell' style={{marginTop:'2rem'}}>
                        <div>
                            
                        <Button variant='danger' style={{float:'right'}} onClick={()=> DeleteBegin()}>Delete PIC</Button>

                        </div>
                        <Button style={{width:'10rem', lineHeight:'1rem'}} variant="dark" onClick={()=>
                                {EditPIC()}}
                                className='LoginButton' > Edit PIC</Button>
                    </div>
                    <Button variant='danger' style={{float:'right'}} onClick={()=> setModalShow(false)}>Close</Button>
                </div>
            </Modal.Body>

        </Modal>



    </div>

</div>




)
}

export default PIC