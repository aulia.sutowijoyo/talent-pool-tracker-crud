import React, {useState} from 'react'
import PlusIcon from '../../assets/Add.svg'
import {getUser} from '../../Redux/action/user'
import { Form, Button, Modal, } from 'react-bootstrap';
import { useDispatch, useSelector } from "react-redux";
import Swal from 'sweetalert2'
import axios from "axios";


function Education() {

const { user } = useSelector((state) => state.userData.userInfo);
const dispatch = useDispatch();
const [modalShow, setModalShow] = useState(false);


const [state, setState] = useState({
name: "",
stack : "",
experience : null,
})

const [idUser, setIdUser] = useState()
const [addItem, setaddItem] = useState(false)
// ===========Add Item==================
const Addbegin = () => {
    if (state.name === "" || state.stack === ""|| state.experience === '') {
    Swal.fire({
        icon: 'warning',
        width: 450,
        confirmButtonText: "ok",
        // confirmButtonColor: "#FF8888",
        // allowEnterKey: false,
        // showCancelButton: true,
        // cancelButtonText: 'Cancel',
        title: 'please fill all input',
        // text: 'Click Outside if you want to Cancel',
        })}else {
            AddTalent()
        }};


const AddTalent = async (e) => {
// eslint-disable-next-line no-unused-vars
try { const res = await axios.post(`
https://glints-team-project-assessment.herokuapp.com/talents`, state,

).then((res) => {
dispatch(getUser())
setaddItem(false)

});
} catch (error) {
console.log('error apa ini',error);
if (error) {
console.log('error loh',error)

Swal.fire({
position: 'center',
icon: 'error',
title: 'Error',
// text: error.response.data.error.errors,
showConfirmButton: false,
timer: 2000
})
}
}
};
// ==============Add End==================
// ===============Edit====================
const EditTalent = async (e) => {
// eslint-disable-next-line no-unused-vars
try { const res = await axios.put(`
https://glints-team-project-assessment.herokuapp.com/talents/${idUser}`, state,

).then((res) => {
dispatch(getUser())
setModalShow(false)

});
} catch (error) {
console.log('error apa ini',error);
if (error) {

Swal.fire({
position: 'center',
icon: 'error',
title: 'Error',
// text: error.response.data.error.errors,
showConfirmButton: false,
timer: 2000
})
}
}
};
// ===============Delete====================
// constDeleteBegin
const DeleteBegin = () => Swal.fire({
icon: 'warning',
width: 450,
confirmButtonText: "Delete??",
confirmButtonColor: "#FF8888",
allowEnterKey: false,
showCancelButton: true,
cancelButtonText: 'Cancel',
title: 'Are You Sure!?',
// text: 'Click Outside if you want to Cancel',
}).then((result) => {
if (result.isConfirmed) {
DeleteTalent()
}
});

const DeleteTalent = async (e) => {
// eslint-disable-next-line no-unused-vars
try { const res = await axios.delete(`
https://glints-team-project-assessment.herokuapp.com/talents/${idUser}`,

).then((res) => {
dispatch(getUser())
setModalShow(false)

});
} catch (error) {
console.log('error apa ini',error);
if (error) {

Swal.fire({
position: 'center',
icon: 'error',
title: 'Error',
// text: error.response.data.error.errors,
showConfirmButton: false,
timer: 2000
})
}
}
};


return (
<div className='InfoContainer'>
    <div className='CareerContainer'>
        <div className='TitleTopContainer mb-4'>
            <div className='TitlePlace' style={{width:'100%', }}>
                <div className='TitleText'>Talent List</div>
                <div className='SubTitleText'>List of Great People</div>
            </div>
            <img onClick={()=> {setaddItem(true); setState({
            name: "",
            stack : "",
            experience : 0,
            })}}
            src={`${PlusIcon}`} alt=''></img>
        </div>
        {/* ========================Add Talent===================== */}

        { addItem ?
        <div className='UpdateCareer'>
            <div className='InfoText'>Talent Name</div>
            <Form.Group className="" controlId="">
                <Form.Control style={{borderColor:'#5C7AE5'}} required className="FormInput" type="text"
                    placeholder="Name" value={state.name} onChange={(e)=> setState({ ...state, name: e.target.value })}
                    />
            </Form.Group>
            <div className='InfoText'>Talent Background</div>

            <Form.Select value={state.stack} onChange={(e)=> setState({ ...state, stack: e.target.value })}
                className="Dropdown me-sm-2" id="PilihSatuan"
                // style={{ borderRadius: "10px" }}
                >
                <option id="PilihItem" value="Backend">
                    Backend
                </option>
                <option id="PilihItem" value="Frontend">
                    Frontend
                </option>
                <option id="PilihItem" value="React Native">
                    React Native
                </option>
                <option id="PilihItem" value="QA">
                    QA
                </option>

            </Form.Select>

            <div className='InfoText'>Total Experience</div>
            <Form.Group className="" controlId="">
                <Form.Control required className="DateInput" type="number" placeholder="Years" value={state.experience}
                    onChange={(e)=> setState({ ...state, experience: e.target.value })}
                    />
            </Form.Group>
            <div style={{marginBottom:'2rem', backgroundColor:'#5C7AE5', width:'100%'}} className="SeperateLine"></div>

            <div className='ButtonCell' style={{marginTop:'2rem'}}>
                <div>
                    <Button variant='danger' style={{float:'right'}} onClick={()=> setaddItem(false)}>Cancel</Button>
                </div>
                <Button style={{width:'10rem', lineHeight:'1rem'}} variant="dark" onClick={()=> {Addbegin()}}
                    className='LoginButton' > Add Talent</Button>
            </div>
        </div>
        : null }

        {/* ========================Mapping===================== */}

        <div className='TableList'>

            <div className='ContentContainer'>

                <div className='MainContent'>

                    <div style={{marginBottom:'2rem', backgroundColor:'#5C7AE5', width:'100%'}}
                        className="SeperateLine"></div>

                    <div className="mappingBox">
                        {user?.data?.map((talent, index)=>(
                        <div className="box" onClick={()=>
                            {setModalShow(true) ;setIdUser(talent?.id);setState({
                            name: talent?.name,
                            stack : talent?.stack,
                            experience : talent?.experience,
                            })}}>
                            <h3 style={{padding:'0', margin:'0'}}> {talent?.name}</h3>
                            <div>stack: {talent?.stack}</div>
                            <div>exp: {talent?.experience} tahun</div>

                        </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>

        {/* ========================Modal Edit Talent===================== */}

        <Modal className='EditModal' size="md" aria-labelledby="contained-modal-title-vcenter" centered show={modalShow}
            onHide={()=> setModalShow(false)}
            >
            <Modal.Body>
                <div className='UpdateCareer'>
                    {user?.data?.filter((data)=> data?.id === idUser).map((item,index)=>(<h1 style={{color:'white'}}>
                        {item?.name}</h1>))}
                    <div className='InfoText'>Talent Name</div>
                    <Form.Group className="" controlId="">
                        <Form.Control style={{borderColor:'#5C7AE5'}} required className="FormInput" type="text"
                            placeholder="Name" value={state.name} onChange={(e)=> setState({ ...state, name:
                            e.target.value })}
                            />
                    </Form.Group>
                    <div className='InfoText'>Talent Background</div>

                    <Form.Select value={state.stack} onChange={(e)=> setState({ ...state, stack:
                        e.target.value })}
                        className="Dropdown me-sm-2" id="PilihSatuan"
                        >
                        <option id="PilihItem" value="Backend">
                            Backend
                        </option>
                        <option id="PilihItem" value="Frontend">
                            Frontend
                        </option>
                        <option id="PilihItem" value="React Native">
                            React Native
                        </option>
                        <option id="PilihItem" value="QA">
                            QA
                        </option>
                    </Form.Select>

                    <div className='InfoText'>Total Experience</div>
                    <Form.Group className="" controlId="">
                        <Form.Control required className="DateInput FormInput" type="number" placeholder="Years"
                            value={state.experience} onChange={(e)=> setState({ ...state, experience: e.target.value })}
                            />
                    </Form.Group>
                    <div style={{marginBottom:'2rem', backgroundColor:'#5C7AE5', width:'100%'}}
                        className="SeperateLine"></div>

                    <div className='ButtonCell' style={{marginTop:'2rem'}}>
                        <div>
                            <Button variant='danger' style={{float:'right'}} onClick={()=> DeleteBegin()}>Delete
                                Talent</Button>
                        </div>
                        <Button style={{width:'10rem', lineHeight:'1rem'}} variant="dark" onClick={()=>
                            {EditTalent()}}
                            className='LoginButton' > Edit Talent</Button>
                    </div>
                    <Button variant='danger' style={{float:'right'}} onClick={()=> setModalShow(false)}>Close</Button>
                </div>
            </Modal.Body>

        </Modal>



    </div>

</div>

)
}

export default Education