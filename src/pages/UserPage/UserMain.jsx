import React, {useState, useEffect} from 'react';
import './UserMain.scss'
import {getUser, getCompany, getPIC, getTRACKER} from '../../Redux/action/user'
import { useDispatch, useSelector } from "react-redux";
import Tracker from './Tracker';
import Company from './Company';
import Talent from './Talent';
import PIC from './PIC';
import Cover from '../../assets/background.jpg'


function UserMain() {
const { user } = useSelector((state) => state.userData.userInfo);
// const { company } = useSelector((state) => state.companyData.companyInfo);
// const { tracker } = useSelector((state) => state.trackerData.trackerInfo);
// const { pic } = useSelector((state) => state.picData.picInfo);


const dispatch = useDispatch();
// console.log(user)
useEffect(() => {
dispatch(getUser());
dispatch(getPIC());
dispatch(getTRACKER());
dispatch(getCompany())}, [dispatch]);


 

const [Page, setPage] = useState (1)
return (
<div>
    <div className='MainContainer'>


        {/* <Col className='RightBox'   > */}
        <div className='CoverImage'>
            <img className='CoverImage' src={`${Cover}`} alt='cover'></img>
        </div>

        <div className="UserContainer">
            {/* <div className="changeCover">
                <button className="changeCoverButton" onClick={()=> seteditCover(true)}
                    style={{float:'right', margin:'-4.7rem -2rem 0 0'}}>
                    <img src={`${PhotoIcon}`} alt=''></img>
                    Change Cover
                </button>
            </div> */}

            {/* <Modal centered show={editCover} onHide={()=> seteditCover(false)}>
                <Modal.Header style={{backgroundColor:'#16162A',border:'none', color:'white'}} closeButton>
                    <Modal.Title>Change Cover</Modal.Title>
                </Modal.Header>
                <Modal.Body style={{backgroundColor:'#16162A', color:'white'}}>
                    <CoverPhoto />
                </Modal.Body>
                <Modal.Footer
                   style={{backgroundColor:'#16162A',border:'none', color:'white'}}>
                    <Button variant="secondary" onClick={()=> seteditCover(false)}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal> */}



            <div className="UserInfo">
                <div style={{fontSize:'32px',fontWeight:'600', color:'#F3F3F3'}}>Dashboard</div>
                <div style={{fontSize:'18px', color:'#8E8EA3', margin:'-7px 0 3rem 0'}}>
                  Lets Define People Future</div>
            </div>
            <div className="switchPageButton">
                <div onClick={()=> setPage(1)} id='LoginButton'>
                    Tracker
                    <div>
                        {Page === 1 ? <div className="ActiveLine"></div> : null}
                    </div>
                </div>
                <div onClick={()=> setPage(2)} id='LoginButton'>
                    Talent
                    <div>
                        {Page === 2 ? <div className="ActiveLine"></div> : null}
                    </div>
                </div>
                <div onClick={()=> setPage(3)} id='LoginButton'>
                Company
                    <div>
                        {Page === 3 ? <div className="ActiveLine"></div> : null}
                    </div>
                </div>
                <div onClick={()=> setPage(4)} id='LoginButton'>
                    PIC
                    <div>
                        {Page === 4 ? <div className="ActiveLine"></div> : null}
                    </div>
                </div>
                {/* <p>{`${Login}`}</p> */}
            </div>
            <div className="UserSeperateLine"></div>
            <div className='contentContainer'>
                {Page === 1 ?
                <Tracker id={user?.data?.user?.id} /> : null}
                {Page === 2 ?
                <Talent /> : null}
                {Page === 3 ?
                <Company /> : null}
                {Page === 4 ?
                <PIC id={user?.data?.user?.id} /> : null}
            </div>
         
        </div>

        {/* </Col> */}

    </div>

</div>
)
}

export default UserMain