/* eslint-disable no-unused-vars */
import React, {useState} from 'react'
import {Col,Modal,Form, Button} from 'react-bootstrap';
import './info.scss'
import './modaledit.scss'
import axios from 'axios';
import {getTRACKER, getUser} from '../../Redux/action/user'
import { useDispatch, useSelector } from "react-redux";
import Edit from '../../assets/edit.svg'
import Swal from 'sweetalert2'
import PlusIcon from '../../assets/Add.svg'


function Tracker() {

const [modalShow, setModalShow] = useState(false);
const [modalTalent, setModalTalent] = useState(false);
const [modalCompany, setModalCompany] = useState(false);
const [modalPIC, setModalPIC] = useState(false);

const { user } = useSelector((state) => state.userData.userInfo);
const { company } = useSelector((state) => state.companyData.companyInfo);
const { tracker } = useSelector((state) => state.trackerData.trackerInfo);
const { pic } = useSelector((state) => state.picData.picInfo);
const dispatch = useDispatch();

// console.log('talent', user)
// console.log('data perusahaan',company)
// console.log('data tracker',tracker)
// console.log('pic', pic)

const [state, setState] = useState({
idTalent: "",
idCompany : "",
idPic : '',
status : 'Review',
})
console.log("cek input",state)
const [userInfo, setUserInfo] = useState({
experience: '',
stack : '',
name: "",
company:""
})

const [companyNeed, setCompanyneed] = useState({
experience: '',
stack : '',
name: "",
})
const [picAssigned, setpicAssigned] = useState({
   
    name: "",
    })
// console.log(companyNeed, userInfo)
const [match, setMatch] = useState()

function talentMatch () {
if (userInfo.experience === companyNeed.experience && userInfo.stack === companyNeed.stack) {setMatch(true)}
if (userInfo.experience !== companyNeed.experience) {setMatch(false)}
if (userInfo.stack !== companyNeed.stack) {setMatch(false)}

}

const [idTracker, setIdTracker] = useState()
const [addItem, setaddItem] = useState(false)
// ===========Add Item==================
const Addbegin = () => {
if (state.idTalent === "" || state.idCompany === ""|| state.idPic === '') {
Swal.fire({
    icon: 'warning',
    width: 450,
    confirmButtonText: "ok",
      title: 'please fill all input',
    })}else {
        AddTracker()
    }};

const AddTracker = async (e) => {
// eslint-disable-next-line no-unused-vars
try { const res = await axios.post(`
https://glints-team-project-assessment.herokuapp.com/trackers`, state,

).then((res) => {
dispatch(getTRACKER())
setaddItem(false)

});
} catch (error) {
console.log('error apa ini',error);
if (error) {
console.log('error loh',error)

Swal.fire({
position: 'center',
icon: 'error',
title: 'Error',
// text: error.response.data.error.errors,
showConfirmButton: false,
timer: 2000
})
}
}
};
// ==============Add End==================
// ===============Edit====================
const EditTalent = async (e) => {
// eslint-disable-next-line no-unused-vars
try { const res = await axios.put(`
https://glints-team-project-assessment.herokuapp.com/trackers/${idTracker}`, state,

).then((res) => {
dispatch(getTRACKER())
setModalShow(false)

});
} catch (error) {
console.log('error apa ini',error);
if (error) {

Swal.fire({
position: 'center',
icon: 'error',
title: 'Error',
// text: error.response.data.error.errors,
showConfirmButton: false,
timer: 2000
})
}
}
};
// ===============Delete====================
// constDeleteBegin
const DeleteBegin = () => Swal.fire({
icon: 'warning',
width: 450,
confirmButtonText: "Delete??",
confirmButtonColor: "#FF8888",
allowEnterKey: false,
showCancelButton: true,
cancelButtonText: 'Cancel',
title: 'Are You Sure!?',
// text: 'Click Outside if you want to Cancel',
}).then((result) => {
if (result.isConfirmed) {
DeleteTalent()
}
});

const DeleteTalent = async (e) => {
// eslint-disable-next-line no-unused-vars
try { const res = await axios.delete(`
https://glints-team-project-assessment.herokuapp.com/trackers/${idTracker}`,

).then((res) => {
dispatch(getUser())
setModalShow(false)

});
} catch (error) {
console.log('error apa ini',error);
if (error) {

Swal.fire({
position: 'center',
icon: 'error',
title: 'Error',
// text: error.response.data.error.errors,
showConfirmButton: false,
timer: 2000
})
}
}
};

return (
<div>
    <div className='InfoContainer'>
        <div className='CareerContainer'>
            <div className='TitleTopContainer mb-4'>
                <div className='TitlePlace' style={{width:'100%', }}>
                    <div className='TitleText'>Tracker List</div>
                    <div className='SubTitleText'>List of Tracker</div>
                </div>
                <img 
                onClick={()=> {setaddItem(true); 
                    setState({...state,
                        idTalent:  "",
                        idPic : "",
                        idCompany : "",
                })
            }}
                src={`${PlusIcon}`} alt=''></img>
            </div>
            {/* ========================Add ===================== */}

            { addItem ?
            <div className='UpdateCareer'>
                <div className='InfoText'>Talent Name</div>
                <Form.Group className="" controlId="">
                    <Form.Control style={{borderColor:'#5C7AE5', background:'transparent'}} required
                        className="FormInput" type="text" placeholder="Name" disabled value={userInfo.name}
                        onChange={(e)=> setState({ ...state, name: e.target.value })}
                        />
                </Form.Group>
                <Button onClick={()=> setModalTalent(true)} variant='outline-light'
                    style={{ margin:'-2rem 0 2rem 0'}}>Choose Talent</Button>
                <Modal className='EditModal' size="lg" aria-labelledby="contained-modal-title-vcenter" centered
                    show={modalTalent} onHide={()=> setModalTalent(false)}>
                    <Modal.Body>

                        <div className="mappingBox">
                            {user?.data?.map((talent, index)=>(
                            <div className="boxTracker" onClick={()=>
                                {
                                // setModalShow(true) ;
                                // setIdUser(talent?.id);
                                setModalTalent(false);
                                setState({...state ,idTalent: talent?.id});
                                setUserInfo({
                                name: talent?.name,
                                stack : talent?.stack,
                                experience : talent?.experience,
                                })}}>
                                <h3 style={{padding:'0', margin:'0'}}> {talent?.name}</h3>
                                <div>stack: {talent?.stack}</div>
                                <div>exp: {talent?.experience} tahun</div>

                            </div>
                            ))}
                        </div>
                    </Modal.Body>
                </Modal>
                {/* <h1> ini company </h1> */}
                <div className='InfoText'>Company Name</div>
                <Form.Group className="" controlId="">
                    <Form.Control style={{borderColor:'#5C7AE5', background:'transparent'}} required
                        className="FormInput" type="text" placeholder="Name" disabled value={companyNeed.name}
                        onChange={(e)=> setState({ ...state, name: e.target.value })}
                        />
                </Form.Group>
                <Button onClick={()=> setModalCompany(true)} variant='outline-light'
                    style={{ margin:'-2rem 0 2rem 0'}}>Choose Company</Button>
                <Modal className='EditModal' size="lg" aria-labelledby="contained-modal-title-vcenter" centered
                    show={modalCompany} onHide={()=> setModalCompany(false)}>
                    <Modal.Body>

                        <div className="mappingBox">
                            {company?.data?.map((company, index)=>(
                            <div className="boxTracker" onClick={()=>
                                {
                                // setModalShow(true) ;
                                // setIdUser(talent?.id);
                                talentMatch ();
                                setModalCompany(false);
                                setState({...state ,idCompany: company?.id});
                                setCompanyneed({
                                name: company?.name,
                                stack : company?.stackNeeded,
                                experience : company?.experienceNeeded,
                                })}}>
                                <h3 style={{padding:'0', margin:'0'}}> {company?.name}</h3>
                                <div>stack needed: {company?.stackNeeded}</div>
                                <div>exp. nedded: {company?.experienceNeeded} tahun</div>

                            </div>
                            ))}
                        </div>
                    </Modal.Body>
                </Modal>
                {/* <h1 style={{color:'white'}}>{`${match}`}</h1> */}
                {match === undefined ? null : 
                
                (match ? <h3 onClick={()=>talentMatch()} style={{color:'white', cursor:'pointer'}}>Company & Talent Match</h3> :
                 <h3 onClick={()=>talentMatch()} style={{color:'white', cursor:'pointer'}}>Company & Talent did not Match</h3>)
                }
                {/* {tracker?.data?.filter((data)=> data?.idCompany?.experienceNeeded ===
                userInfo?.experience).map((company,index)=>(
                <h1 style={{color:'white'}}>
                    namanya {company?.stat}
                </h1>
                ))} */}

                <div style={{marginTop:'2rem'}} className='InfoText'>Select PIC</div>
                <Form.Group className="" controlId="">
                    <Form.Control style={{borderColor:'#5C7AE5', background:'transparent'}} required
                        className="FormInput" type="text" placeholder="PIC" disabled value={picAssigned.name}
                       
                        />
                </Form.Group>
                <Button onClick={()=> setModalPIC(true)} variant='outline-light'
                    style={{ margin:'-2rem 0 2rem 0'}}>Choose PIC</Button>
                <Modal className='EditModal' size="lg" aria-labelledby="contained-modal-title-vcenter" centered
                    show={modalPIC} onHide={()=> setModalPIC(false)}>
                    <Modal.Body>

                        <div className="mappingBox">
                            {pic?.data?.map((pic, index)=>(
                            <div className="boxTracker" onClick={()=>
                                {
                                // setModalShow(true) ;
                                // setIdUser(talent?.id);
                                // talentMatch ();
                                setModalPIC(false);
                                setState({...state ,idPic: pic?.id});
                                setpicAssigned({
                                name: pic?.name,
               
                                })
                                }}>
                                <h3 style={{padding:'0', margin:'0'}}> {pic?.name}</h3>
                                {/* <div>stack: {talent?.stack}</div>
                                <div>exp: {talent?.experience} tahun</div> */}

                            </div>
                            ))}
                        </div>
                    </Modal.Body>
                </Modal>

                           
                <div style={{marginBottom:'2rem', backgroundColor:'#5C7AE5', width:'100%'}} className="SeperateLine">
                </div>

                <div className='ButtonCell' style={{marginTop:'2rem'}}>
                    <div>
                        <Button variant='danger' style={{float:'right'}} onClick={()=>
                            setaddItem(false)}>Cancel</Button>
                    </div>
                    <Button style={{width:'10rem', lineHeight:'1rem'}} variant="dark" onClick={()=> {Addbegin()}}
                        className='LoginButton' > Add Tracker</Button>
                </div>
            </div>
            : null }

            {/* ========================Mapping===================== */}

            <div className='TableList'>

                <div className='ContentContainer'>

                    <div className='MainContent'>
         
                        <div style={{marginBottom:'2rem', backgroundColor:'#5C7AE5', width:'100%'}}
                            className="SeperateLine"></div>
                             <div className="mappingBox">
                        {tracker?.data?.map((tracker, index)=>(
                        <div className="box" style={{height:'7.5rem'}} onClick={()=>
                            {setModalShow(true) ;
                                setIdTracker(tracker?.id);
                                setUserInfo({...userInfo,
                                    name : tracker?.idTalent?.name,
                                    company :tracker?.idCompany?.name
                                });
                                setState({...state,
                                    idTalent: tracker?.idTalent?.id,
                                    idCompany : tracker?.idCompany?.id,
                                    idPic : tracker?.idPic?.id,
                            })
                            }}>
                            <h3 style={{padding:'0', margin:'0'}}> {tracker?.idTalent?.name}</h3>
                            <div>Company: {tracker?.idCompany?.name}</div>
                            <div>PIC: {tracker?.idPic?.name} </div>
                            <div>Status: {tracker?.status} </div>

                        </div>
                        ))}
                    </div>
                      
                    </div>

                </div>
            </div>

            {/* ========================Modal Edit Talent===================== */}

            <Modal className='EditModal' size="md" aria-labelledby="contained-modal-title-vcenter" centered
                show={modalShow} onHide={()=> setModalShow(false)}
                >
                <Modal.Body>
                    <div className='UpdateCareer'>
                      
                            {/* <h3>{idTracker}</h3> */}
                            <h1 style={{color:'white'}}>{userInfo?.name} - {userInfo?.company}  </h1>
                        <div className='InfoText'>Talent Status</div>
                       <Form.Select value={state.status} onChange={(e)=> setState({ ...state, status: e.target.value })}
                    className="Dropdown me-sm-2" id="PilihSatuan"
                    // style={{ borderRadius: "10px" }}
                    >
                    <option id="PilihItem" value="Review">
                        Review
                    </option>
                    <option id="PilihItem" value="HR Interview">
                        HR Interview
                    </option>
                    <option id="PilihItem" value="User Interview">
                        User Interview
                    </option>
                    <option id="PilihItem" value="offer">
                        offer
                    </option>
                    <option id="PilihItem" value="accepted">
                        accepted
                    </option>
                    <option id="PilihItem" value="rejected">
                        rejected
                    </option>
                </Form.Select>
                        <div style={{marginBottom:'2rem', backgroundColor:'#5C7AE5', width:'100%'}}
                            className="SeperateLine"></div>

                        <div className='ButtonCell' style={{marginTop:'2rem'}}>
                            <div>
                       
                            </div>
                            <Button style={{width:'10rem', lineHeight:'1rem'}} variant="dark" onClick={()=>
                                {EditTalent()}}
                                className='LoginButton' > Update Status</Button>
                        </div>
                        <Button variant='danger' style={{float:'right'}} onClick={()=>
                            setModalShow(false)}>Close</Button>
                    </div>
                </Modal.Body>

            </Modal>



        </div>
    </div>
</div>
)
}

export default Tracker