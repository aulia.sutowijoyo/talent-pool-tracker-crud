
import Router from './Route/router';
import { BrowserRouter } from "react-router-dom";
// import Bar from './pages/Bar';

function App() {
  return (
    <div className="App">
      {/* <Bar /> */}
       <BrowserRouter>
         <Router />
       </BrowserRouter>
    </div>
  );
}

export default App;
